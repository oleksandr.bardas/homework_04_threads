package org.example;

import org.example.macdac.MacDacDemo;
import org.example.warcraft.WarCraftDemo;

public class App {
    public static void main(String[] args) {
        System.out.println("===================================WarCraft_demo===================================");
        //new WarCraftDemo().demo();

        System.out.println("===================================MacDac_demo=====================================");
        new MacDacDemo().demo();
    }
}
