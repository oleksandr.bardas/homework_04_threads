package org.example.macdac;

public class Cashier {
    private boolean closed;

    public boolean isClosed() {
        return closed;
    }

    public void handleOrder(int visitorId) {
        if (!closed) {
            try {
                Thread.sleep(3 + (long) (Math.random() * 8));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Visitor " + visitorId + " has got his order");
            closed = Math.random() > 0.5;
            if (closed) {
                System.out.println("We are closed");
            }
        }
    }
}
