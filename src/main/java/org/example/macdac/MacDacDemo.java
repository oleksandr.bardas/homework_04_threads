package org.example.macdac;

public class MacDacDemo {
    public void demo() {
        Cashier cashier = new Cashier();
        for (int i = 0; i < 5; i++) {
            new Visitor(cashier, i);
        }
    }
}
