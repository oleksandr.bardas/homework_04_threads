package org.example.macdac;

public class Visitor extends Thread {
    private final Cashier cashier;
    private int index;

    public Visitor(Cashier cashier, int index) {
        this.cashier = cashier;
        this.index = index;
        start();
    }

    public void run() {
        synchronized (cashier) {
            if (!cashier.isClosed()) {
                cashier.handleOrder(index);
            }
        }
    }
}
