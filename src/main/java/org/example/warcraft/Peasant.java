package org.example.warcraft;

import lombok.Getter;

public class Peasant extends Thread {
    @Getter
    private int mined;
    private final GoldMine goldMine;

    public Peasant(GoldMine goldMine) {
        this.goldMine = goldMine;
        start();
    }

    public void run() {
        mine();
    }

    public void mine() {
        int mined;
        int portion = 3;
        while ((mined = goldMine.take(portion)) > 0) {
            this.mined += mined;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
