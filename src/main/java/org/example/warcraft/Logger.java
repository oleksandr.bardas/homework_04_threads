package org.example.warcraft;

import java.util.List;

public class Logger extends Thread {
    private final List<Peasant> peasants;
    private final GoldMine goldMine;

    public Logger(List<Peasant> peasants, GoldMine goldMine) {
        this.peasants = peasants;
        this.goldMine = goldMine;
        start();
    }

    public void run() {
        while (goldMine.getRest() > 0) {
            log();
        }
    }

    public void log() {
        for (int i = 0; i < peasants.size(); i++) {
            System.out.println("Peasant #" + i + ": " + peasants.get(i).getMined());
        }
        System.out.println("Mine rest: " + goldMine.getRest());
    }
}
