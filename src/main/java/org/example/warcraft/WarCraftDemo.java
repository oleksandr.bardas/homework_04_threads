package org.example.warcraft;

import java.util.ArrayList;
import java.util.List;

public class WarCraftDemo {
    public void demo() {
        GoldMine mine = new GoldMine(1000);

        List<Peasant> peasants = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            peasants.add(new Peasant(mine));
        }

        Barrack barrack = new Barrack(peasants, mine);
        Logger logger = new Logger(peasants, mine);

    }
}
