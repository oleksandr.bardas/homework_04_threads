package org.example.warcraft;

import java.util.List;

public class Barrack extends Thread {
    private final List<Peasant> peasants;
    private final GoldMine goldMine;

    public Barrack(List<Peasant> peasants, GoldMine goldMine) {
        this.peasants = peasants;
        this.goldMine = goldMine;
        start();
    }

    public void run() {
        while (goldMine.getRest() > 0) {
            peasants.add(next());
        }
    }

    public Peasant next() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return new Peasant(goldMine);
    }
}
