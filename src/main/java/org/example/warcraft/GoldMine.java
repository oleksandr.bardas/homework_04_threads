package org.example.warcraft;

import lombok.*;

@EqualsAndHashCode
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class GoldMine {
    private int rest;

    public synchronized int take(int portion) {
        if (rest > portion) {
            rest -= portion;
        } else {
            portion = rest;
            rest = 0;
        }
        return portion;
    }

    public synchronized int getRest(){
        return rest;
    }
}
